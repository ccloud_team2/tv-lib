<?php
/**
 * @file
 * tvlib_feature.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function tvlib_feature_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-administration_:admin/people
  $menu_links['menu-site-administration_:admin/people'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'admin/people',
    'router_path' => 'admin/people',
    'link_title' => '系统管理',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:admin/people',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-site-administration_:admin/people/create
  $menu_links['menu-site-administration_:admin/people/create'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'admin/people/create',
    'router_path' => 'admin/people/create',
    'link_title' => '添加新用户',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:admin/people/create',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:admin/people',
  );
  // Exported menu link: menu-site-administration_:admin/people/subpermissions
  $menu_links['menu-site-administration_:admin/people/subpermissions'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'admin/people/subpermissions',
    'router_path' => 'admin/people/subpermissions',
    'link_title' => '编辑角色权限',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:admin/people/subpermissions',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:admin/people',
  );
  // Exported menu link: menu-site-administration_:admin/reports/status/run-cron
  $menu_links['menu-site-administration_:admin/reports/status/run-cron'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'admin/reports/status/run-cron',
    'router_path' => 'admin/reports/status/run-cron',
    'link_title' => '运行系统任务',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-site-administration_:admin/reports/status/run-cron',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:admin/people',
  );
  // Exported menu link: menu-site-administration_:admin/structure/taxonomy/category
  $menu_links['menu-site-administration_:admin/structure/taxonomy/category'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'admin/structure/taxonomy/category',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => '分类目录',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:admin/structure/taxonomy/category',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:admin/people',
  );
  // Exported menu link: menu-site-administration_:admin/structure/taxonomy/system_settings
  $menu_links['menu-site-administration_:admin/structure/taxonomy/system_settings'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'admin/structure/taxonomy/system_settings',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => '系统参数配置',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:admin/structure/taxonomy/system_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:admin/people',
  );
  // Exported menu link: menu-site-administration_:admin/structure/types
  $menu_links['menu-site-administration_:admin/structure/types'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'admin/structure/types',
    'router_path' => 'admin/structure/types',
    'link_title' => '编辑模版',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:admin/structure/types',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:admin/people',
  );
  // Exported menu link: menu-site-administration_:content-admin/image
  $menu_links['menu-site-administration_:content-admin/image'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'content-admin/image',
    'router_path' => 'content-admin/%',
    'link_title' => '图片管理',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'query' => array(
        'type[0]' => 'info',
        'type[1]' => 'announcement',
        'type[2]' => 'book',
        'type[3]' => 'poster',
      ),
      'identifier' => 'menu-site-administration_:content-admin/image',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-site-administration_:content-admin/video-audio
  $menu_links['menu-site-administration_:content-admin/video-audio'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'content-admin/video-audio',
    'router_path' => 'content-admin/%',
    'link_title' => '视音频管理',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'query' => array(
        'type[0]' => 'video',
      ),
      'identifier' => 'menu-site-administration_:content-admin/video-audio',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-site-administration_:content-statistic
  $menu_links['menu-site-administration_:content-statistic'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'content-statistic',
    'router_path' => 'content-statistic',
    'link_title' => '内容数据分项统计',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:content-statistic',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:admin/people',
  );
  // Exported menu link: menu-site-administration_:create-image
  $menu_links['menu-site-administration_:create-image'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'create-image',
    'router_path' => 'create-image',
    'link_title' => '添加图片',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:create-image',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:content-admin/image',
  );
  // Exported menu link: menu-site-administration_:file/add
  $menu_links['menu-site-administration_:file/add'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'file/add',
    'router_path' => 'file/add',
    'link_title' => '上传图片',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:file/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:content-admin/image',
  );
  // Exported menu link: menu-site-administration_:import-image
  $menu_links['menu-site-administration_:import-image'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'import-image',
    'router_path' => 'import-image',
    'link_title' => '导入图片信息',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:import-image',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:content-admin/image',
  );
  // Exported menu link: menu-site-administration_:node/add/announcement
  $menu_links['menu-site-administration_:node/add/announcement'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'node/add/announcement',
    'router_path' => 'node/add/announcement',
    'link_title' => '添加公告',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:node/add/announcement',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:create-image',
  );
  // Exported menu link: menu-site-administration_:node/add/announcement-import
  $menu_links['menu-site-administration_:node/add/announcement-import'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'node/add/announcement-import',
    'router_path' => 'node/add/announcement-import',
    'link_title' => '导入公告信息',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:node/add/announcement-import',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:import-image',
  );
  // Exported menu link: menu-site-administration_:node/add/book
  $menu_links['menu-site-administration_:node/add/book'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'node/add/book',
    'router_path' => 'node/add/book',
    'link_title' => '添加图书',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:node/add/book',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:create-image',
  );
  // Exported menu link: menu-site-administration_:node/add/book-import
  $menu_links['menu-site-administration_:node/add/book-import'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'node/add/book-import',
    'router_path' => 'node/add/book-import',
    'link_title' => '导入图书信息',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:node/add/book-import',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:import-image',
  );
  // Exported menu link: menu-site-administration_:node/add/info
  $menu_links['menu-site-administration_:node/add/info'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'node/add/info',
    'router_path' => 'node/add/info',
    'link_title' => '添加内容',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:node/add/info',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:create-image',
  );
  // Exported menu link: menu-site-administration_:node/add/info-import
  $menu_links['menu-site-administration_:node/add/info-import'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'node/add/info-import',
    'router_path' => 'node/add/info-import',
    'link_title' => '导入内容信息',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:node/add/info-import',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:import-image',
  );
  // Exported menu link: menu-site-administration_:node/add/poster
  $menu_links['menu-site-administration_:node/add/poster'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'node/add/poster',
    'router_path' => 'node/add/poster',
    'link_title' => '添加海报',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:node/add/poster',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:create-image',
  );
  // Exported menu link: menu-site-administration_:node/add/poster-import
  $menu_links['menu-site-administration_:node/add/poster-import'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'node/add/poster-import',
    'router_path' => 'node/add/poster-import',
    'link_title' => '导入海报信息',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:node/add/poster-import',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:import-image',
  );
  // Exported menu link: menu-site-administration_:period-statistic
  $menu_links['menu-site-administration_:period-statistic'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'period-statistic',
    'router_path' => 'period-statistic',
    'link_title' => '日志统计',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:period-statistic',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:admin/people',
  );
  // Exported menu link: menu-site-administration_:user
  $menu_links['menu-site-administration_:user'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'user',
    'router_path' => 'user',
    'link_title' => '修改密码',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:user',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:welcome',
  );
  // Exported menu link: menu-site-administration_:user/logout
  $menu_links['menu-site-administration_:user/logout'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => '退出',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:user/logout',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-site-administration_:welcome',
  );
  // Exported menu link: menu-site-administration_:welcome
  $menu_links['menu-site-administration_:welcome'] = array(
    'menu_name' => 'menu-site-administration',
    'link_path' => 'welcome',
    'router_path' => 'welcome',
    'link_title' => '用户登陆管理',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-administration_:welcome',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('上传图片');
  t('修改密码');
  t('内容数据分项统计');
  t('分类目录');
  t('图片管理');
  t('导入公告信息');
  t('导入内容信息');
  t('导入图书信息');
  t('导入图片信息');
  t('导入海报信息');
  t('日志统计');
  t('添加公告');
  t('添加内容');
  t('添加图书');
  t('添加图片');
  t('添加新用户');
  t('添加海报');
  t('用户登陆管理');
  t('系统参数配置');
  t('系统管理');
  t('编辑模版');
  t('编辑角色权限');
  t('视音频管理');
  t('运行系统任务');
  t('退出');


  return $menu_links;
}
