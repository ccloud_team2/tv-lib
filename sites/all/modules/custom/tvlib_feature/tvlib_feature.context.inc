<?php
/**
 * @file
 * tvlib_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function tvlib_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'announcement';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'content-admin/announcement' => 'content-admin/announcement',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '公告查询',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['announcement'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'book';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'content-admin/book' => 'content-admin/book',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '图书查询',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['book'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'content_publish';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'content-admin/content-publish' => 'content-admin/content-publish',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '已发布内容',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['content_publish'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'content_types';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'admin/structure/types' => 'admin/structure/types',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '编辑模版',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['content_types'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'content_unpublish';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'content-admin/content-unpublish' => 'content-admin/content-unpublish',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '未发布内容',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['content_unpublish'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_announcement';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add/announcement' => 'node/add/announcement',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '添加公告',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['create_announcement'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_announcement_import';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add/announcement-import' => 'node/add/announcement-import',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '导入公告信息',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['create_announcement_import'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_audio';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add/audio' => 'node/add/audio',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '添加音频',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['create_audio'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_book';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add/book' => 'node/add/book',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '添加图书',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['create_book'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_book_import';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add/book-import' => 'node/add/book-import',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '导入图书信息',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['create_book_import'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_info';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add/info' => 'node/add/info',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '添加内容',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['create_info'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_info_import';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add/info-import' => 'node/add/info-import',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '导入内容信息',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['create_info_import'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_poster';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add/poster' => 'node/add/poster',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '添加海报',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['create_poster'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_poster_import';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add/poster-import' => 'node/add/poster-import',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '导入海报信息',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['create_poster_import'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_user';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'admin/people/create' => 'admin/people/create',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '添加新用户',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['create_user'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_video';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add/video' => 'node/add/video',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '添加视频',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['create_video'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'create_video_audio';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add/video-audio' => 'node/add/video-audio',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '添加视音频',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['create_video_audio'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'edit_user';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'user/*/edit' => 'user/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '修改密码',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['edit_user'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'image';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'content-admin/image' => 'content-admin/image',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '图片查询',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['image'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'info';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'content-admin/info' => 'content-admin/info',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '内容查询',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['info'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'manage_display';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'admin/structure/types/manage/*/display' => 'admin/structure/types/manage/*/display',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '预览模板',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['manage_display'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'manage_fields';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'admin/structure/types/manage/*/fields' => 'admin/structure/types/manage/*/fields',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '编辑模板',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['manage_fields'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'permission';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'admin/people/permissions' => 'admin/people/permissions',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '编辑角色权限',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['permission'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'poster';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'content-admin/poster' => 'content-admin/poster',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '海报查询',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['poster'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'upload_file';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'file/add' => 'file/add',
        'file/add/*' => 'file/add/*',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '上传图片',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['upload_file'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'video_audio';
  $context->description = '';
  $context->tag = 'Title override';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'content-admin/video-audio' => 'content-admin/video-audio',
      ),
    ),
  );
  $context->reactions = array(
    'title_override' => array(
      'admin' => TRUE,
      'title' => '视音频查询',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Title override');
  $export['video_audio'] = $context;

  return $export;
}
