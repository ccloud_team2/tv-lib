<?php
/**
 * @file
 * tvlib_feature.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function tvlib_feature_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'content_approval';
  $mini->category = '';
  $mini->admin_title = 'Content approval';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array(
    0 => array(
      'identifier' => 'User',
      'keyword' => 'user',
      'name' => 'user',
      'type' => 'current',
      'uid' => '',
      'id' => 1,
    ),
  );
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'threecol_33_34_33_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '00420632-d767-4866-86ad-222bc734e6d0';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1ed39c26-1a00-4595-a366-46cc9ff5af1f';
    $pane->panel = 'left';
    $pane->type = 'rules_panes';
    $pane->subtype = 'rules_approve_content';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'perm',
          'settings' => array(
            'perm' => 'use Rules component rules_approve_content',
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'entity_field_value:node:video:field_status',
          'settings' => array(
            'field_status' => array(
              'und' => array(
                0 => array(
                  'value' => '未审核',
                ),
              ),
            ),
            'field_status_value' => '未审核',
          ),
          'context' => 'requiredcontext_entity:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    );
    $pane->configuration = array(
      'override_title' => 1,
      'button_text' => '确认通过审核',
      'help_text' => '',
      'component' => 'rules_approve_content',
      'context' => array(
        0 => 'requiredcontext_entity:node_1',
      ),
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1ed39c26-1a00-4595-a366-46cc9ff5af1f';
    $display->content['new-1ed39c26-1a00-4595-a366-46cc9ff5af1f'] = $pane;
    $display->panels['left'][0] = 'new-1ed39c26-1a00-4595-a366-46cc9ff5af1f';
    $pane = new stdClass();
    $pane->pid = 'new-4b3e8c8d-f070-4d1f-a9b9-afeeaa4ca7b9';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'perm',
          'settings' => array(
            'perm' => 'use Rules component rules_approve_content',
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'entity_field_value:node:book:field_status',
          'settings' => array(
            'field_status' => array(
              'und' => array(
                0 => array(
                  'value' => '未审核',
                ),
              ),
            ),
            'field_status_value' => '未审核',
          ),
          'context' => 'requiredcontext_entity:node_1',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'admin_title' => '审核不通过',
      'title' => ' ',
      'body' => '<?php
  $nid = %node:nid;
  $form = drupal_get_form(\'tvlib_reject\', $nid);
  print drupal_render($form);
?>',
      'format' => 'php_code',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4b3e8c8d-f070-4d1f-a9b9-afeeaa4ca7b9';
    $display->content['new-4b3e8c8d-f070-4d1f-a9b9-afeeaa4ca7b9'] = $pane;
    $display->panels['middle'][0] = 'new-4b3e8c8d-f070-4d1f-a9b9-afeeaa4ca7b9';
    $pane = new stdClass();
    $pane->pid = 'new-8583ccbb-9555-40e5-96ec-b083370a12be';
    $pane->panel = 'top';
    $pane->type = 'token';
    $pane->subtype = 'user:name';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_field_value:node:video:field_status',
          'settings' => array(
            'field_status' => array(
              'und' => array(
                0 => array(
                  'value' => '未审核',
                ),
              ),
            ),
            'field_status_value' => '未审核',
          ),
          'context' => 'requiredcontext_entity:node_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'perm',
          'settings' => array(
            'perm' => 'use Rules component rules_approve_content',
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'sanitize' => 1,
      'context' => 'context_user_1',
      'override_title' => 1,
      'override_title_text' => '审核人',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8583ccbb-9555-40e5-96ec-b083370a12be';
    $display->content['new-8583ccbb-9555-40e5-96ec-b083370a12be'] = $pane;
    $display->panels['top'][0] = 'new-8583ccbb-9555-40e5-96ec-b083370a12be';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-1ed39c26-1a00-4595-a366-46cc9ff5af1f';
  $mini->display = $display;
  $export['content_approval'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'term_distribute';
  $mini->category = '';
  $mini->admin_title = 'Term distribute';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Taxonomy term',
      'keyword' => 'taxonomy_term',
      'name' => 'entity:taxonomy_term',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '00420632-d767-4866-86ad-222bc734e6d0';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-51df1fa2-47be-4bbc-aa27-7e21ae49a06a';
    $pane->panel = 'center';
    $pane->type = 'rules_panes';
    $pane->subtype = 'rules_distribute_content_add_term';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_field_value:taxonomy_term:category:field_status',
          'settings' => array(
            'field_status' => array(
              'und' => array(
                0 => array(
                  'value' => '未分发',
                ),
              ),
            ),
            'field_status_value' => '未分发',
          ),
          'context' => 'requiredcontext_entity:taxonomy_term_1',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 1,
      'button_text' => '提交到广电',
      'help_text' => '',
      'component' => 'rules_distribute_content_add_term',
      'context' => array(
        0 => 'requiredcontext_entity:taxonomy_term_1',
      ),
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '51df1fa2-47be-4bbc-aa27-7e21ae49a06a';
    $display->content['new-51df1fa2-47be-4bbc-aa27-7e21ae49a06a'] = $pane;
    $display->panels['center'][0] = 'new-51df1fa2-47be-4bbc-aa27-7e21ae49a06a';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['term_distribute'] = $mini;

  return $export;
}
