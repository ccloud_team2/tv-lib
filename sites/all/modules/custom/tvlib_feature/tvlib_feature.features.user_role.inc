<?php
/**
 * @file
 * tvlib_feature.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function tvlib_feature_user_default_roles() {
  $roles = array();

  // Exported role: 分发员.
  $roles['分发员'] = array(
    'name' => '分发员',
    'weight' => 4,
  );

  // Exported role: 审核员.
  $roles['审核员'] = array(
    'name' => '审核员',
    'weight' => 5,
  );

  // Exported role: 开发人员.
  $roles['开发人员'] = array(
    'name' => '开发人员',
    'weight' => 2,
  );

  // Exported role: 游客.
  $roles['游客'] = array(
    'name' => '游客',
    'weight' => 3,
  );

  // Exported role: 管理员.
  $roles['管理员'] = array(
    'name' => '管理员',
    'weight' => 6,
  );

  return $roles;
}
