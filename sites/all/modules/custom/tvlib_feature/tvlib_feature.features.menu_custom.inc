<?php
/**
 * @file
 * tvlib_feature.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function tvlib_feature_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-site-administration.
  $menus['menu-site-administration'] = array(
    'menu_name' => 'menu-site-administration',
    'title' => '管理菜单',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('管理菜单');


  return $menus;
}
