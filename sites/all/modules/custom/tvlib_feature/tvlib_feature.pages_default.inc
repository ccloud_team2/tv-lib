<?php
/**
 * @file
 * tvlib_feature.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function tvlib_feature_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Video',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'User',
        'keyword' => 'user',
        'name' => 'user',
        'type' => 'current',
        'uid' => '',
        'id' => 1,
      ),
    ),
    'relationships' => array(
      0 => array(
        'identifier' => 'Approver',
        'keyword' => 'approver',
        'name' => 'entity_from_field:field_er_user-node-user',
        'delta' => 0,
        'context' => 'argument_entity_id:node_1',
        'id' => 1,
      ),
      1 => array(
        'identifier' => 'Thumbnail',
        'keyword' => 'thumbnail',
        'name' => 'entity_from_field:field_er_picture-node-node',
        'delta' => 0,
        'context' => 'argument_entity_id:node_1',
        'id' => 1,
      ),
    ),
    'access' => array(
      'logic' => 'and',
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'video' => 'video',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '3d4b4a30-c3e9-4701-9c52-549f903d116a';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-c6494491-1e29-4e37-8e9a-a1e3475bef16';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_status';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'list_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c6494491-1e29-4e37-8e9a-a1e3475bef16';
    $display->content['new-c6494491-1e29-4e37-8e9a-a1e3475bef16'] = $pane;
    $display->panels['center'][0] = 'new-c6494491-1e29-4e37-8e9a-a1e3475bef16';
    $pane = new stdClass();
    $pane->pid = 'new-13186f0a-4011-4fe2-b0b8-c8e263610e68';
    $pane->panel = 'center';
    $pane->type = 'node_author';
    $pane->subtype = 'node_author';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '上传者',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '13186f0a-4011-4fe2-b0b8-c8e263610e68';
    $display->content['new-13186f0a-4011-4fe2-b0b8-c8e263610e68'] = $pane;
    $display->panels['center'][1] = 'new-13186f0a-4011-4fe2-b0b8-c8e263610e68';
    $pane = new stdClass();
    $pane->pid = 'new-5cdb38eb-7d05-4afc-bcad-b52b742100c9';
    $pane->panel = 'center';
    $pane->type = 'token';
    $pane->subtype = 'user:name';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_field_value:node:video:field_status',
          'settings' => array(
            'field_status' => array(
              'und' => array(
                0 => array(
                  'value' => '未审核',
                ),
              ),
            ),
            'field_status_value' => '未审核',
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'sanitize' => 1,
      'context' => 'relationship_entity_from_field:field_er_user-node-user_1',
      'override_title' => 1,
      'override_title_text' => '审核人',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '5cdb38eb-7d05-4afc-bcad-b52b742100c9';
    $display->content['new-5cdb38eb-7d05-4afc-bcad-b52b742100c9'] = $pane;
    $display->panels['center'][2] = 'new-5cdb38eb-7d05-4afc-bcad-b52b742100c9';
    $pane = new stdClass();
    $pane->pid = 'new-6f6f5e92-d722-4d0c-b5b0-79bed496de07';
    $pane->panel = 'center';
    $pane->type = 'token';
    $pane->subtype = 'user:name';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_field_value:node:video:field_status',
          'settings' => array(
            'field_status' => array(
              'und' => array(
                0 => array(
                  'value' => '未审核',
                ),
              ),
            ),
            'field_status_value' => '未审核',
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'sanitize' => 1,
      'context' => 'context_user_1',
      'override_title' => 1,
      'override_title_text' => '审核人',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '6f6f5e92-d722-4d0c-b5b0-79bed496de07';
    $display->content['new-6f6f5e92-d722-4d0c-b5b0-79bed496de07'] = $pane;
    $display->panels['center'][3] = 'new-6f6f5e92-d722-4d0c-b5b0-79bed496de07';
    $pane = new stdClass();
    $pane->pid = 'new-20af9cb8-3618-4b83-937d-759e95764c47';
    $pane->panel = 'center';
    $pane->type = 'rules_panes';
    $pane->subtype = 'rules_approve_content';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_field_value:node:video:field_status',
          'settings' => array(
            'field_status' => array(
              'und' => array(
                0 => array(
                  'value' => '未审核',
                ),
              ),
            ),
            'field_status_value' => '未审核',
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'perm',
          'settings' => array(
            'perm' => 'use Rules component rules_approve_content',
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 1,
      'button_text' => '确认通过审核',
      'help_text' => '',
      'component' => 'rules_approve_content',
      'context' => array(
        0 => 'argument_entity_id:node_1',
      ),
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '20af9cb8-3618-4b83-937d-759e95764c47';
    $display->content['new-20af9cb8-3618-4b83-937d-759e95764c47'] = $pane;
    $display->panels['center'][4] = 'new-20af9cb8-3618-4b83-937d-759e95764c47';
    $pane = new stdClass();
    $pane->pid = 'new-411af03e-ce18-4989-a26f-e6c1e02ee931';
    $pane->panel = 'center';
    $pane->type = 'panels_mini';
    $pane->subtype = 'content_approval';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:node_1',
      ),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '411af03e-ce18-4989-a26f-e6c1e02ee931';
    $display->content['new-411af03e-ce18-4989-a26f-e6c1e02ee931'] = $pane;
    $display->panels['center'][5] = 'new-411af03e-ce18-4989-a26f-e6c1e02ee931';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context_2';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Default',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'User',
        'keyword' => 'user',
        'name' => 'user',
        'type' => 'current',
        'uid' => '',
        'id' => 1,
      ),
    ),
    'relationships' => array(
      0 => array(
        'identifier' => 'Approver',
        'keyword' => 'approver',
        'name' => 'entity_from_field:field_er_user-node-user',
        'delta' => 0,
        'context' => 'argument_entity_id:node_1',
        'id' => 1,
      ),
    ),
    'access' => array(
      'logic' => 'and',
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'announcement' => 'announcement',
              'audio' => 'audio',
              'book' => 'book',
              'info' => 'info',
              'poster' => 'poster',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '3d4b4a30-c3e9-4701-9c52-549f903d116a';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-c6494491-1e29-4e37-8e9a-a1e3475bef16';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_status';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'list_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c6494491-1e29-4e37-8e9a-a1e3475bef16';
    $display->content['new-c6494491-1e29-4e37-8e9a-a1e3475bef16'] = $pane;
    $display->panels['center'][0] = 'new-c6494491-1e29-4e37-8e9a-a1e3475bef16';
    $pane = new stdClass();
    $pane->pid = 'new-13186f0a-4011-4fe2-b0b8-c8e263610e68';
    $pane->panel = 'center';
    $pane->type = 'node_author';
    $pane->subtype = 'node_author';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '上传者',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '13186f0a-4011-4fe2-b0b8-c8e263610e68';
    $display->content['new-13186f0a-4011-4fe2-b0b8-c8e263610e68'] = $pane;
    $display->panels['center'][1] = 'new-13186f0a-4011-4fe2-b0b8-c8e263610e68';
    $pane = new stdClass();
    $pane->pid = 'new-6f6f5e92-d722-4d0c-b5b0-79bed496de07';
    $pane->panel = 'center';
    $pane->type = 'token';
    $pane->subtype = 'user:name';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_field_value:node:video:field_status',
          'settings' => array(
            'field_status' => array(
              'und' => array(
                0 => array(
                  'value' => '未审核',
                ),
              ),
            ),
            'field_status_value' => '未审核',
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'sanitize' => 1,
      'context' => 'context_user_1',
      'override_title' => 1,
      'override_title_text' => '审核人',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '6f6f5e92-d722-4d0c-b5b0-79bed496de07';
    $display->content['new-6f6f5e92-d722-4d0c-b5b0-79bed496de07'] = $pane;
    $display->panels['center'][2] = 'new-6f6f5e92-d722-4d0c-b5b0-79bed496de07';
    $pane = new stdClass();
    $pane->pid = 'new-5cdb38eb-7d05-4afc-bcad-b52b742100c9';
    $pane->panel = 'center';
    $pane->type = 'token';
    $pane->subtype = 'user:name';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_field_value:node:video:field_status',
          'settings' => array(
            'field_status' => array(
              'und' => array(
                0 => array(
                  'value' => '未审核',
                ),
              ),
            ),
            'field_status_value' => '未审核',
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'sanitize' => 1,
      'context' => 'relationship_entity_from_field:field_er_user-node-user_1',
      'override_title' => 1,
      'override_title_text' => '审核人',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '5cdb38eb-7d05-4afc-bcad-b52b742100c9';
    $display->content['new-5cdb38eb-7d05-4afc-bcad-b52b742100c9'] = $pane;
    $display->panels['center'][3] = 'new-5cdb38eb-7d05-4afc-bcad-b52b742100c9';
    $pane = new stdClass();
    $pane->pid = 'new-20af9cb8-3618-4b83-937d-759e95764c47';
    $pane->panel = 'center';
    $pane->type = 'rules_panes';
    $pane->subtype = 'rules_approve_content';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_field_value:node:video:field_status',
          'settings' => array(
            'field_status' => array(
              'und' => array(
                0 => array(
                  'value' => '未审核',
                ),
              ),
            ),
            'field_status_value' => '未审核',
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'perm',
          'settings' => array(
            'perm' => 'use Rules component rules_approve_content',
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 1,
      'button_text' => '确认通过审核',
      'help_text' => '',
      'component' => 'rules_approve_content',
      'context' => array(
        0 => 'argument_entity_id:node_1',
      ),
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '20af9cb8-3618-4b83-937d-759e95764c47';
    $display->content['new-20af9cb8-3618-4b83-937d-759e95764c47'] = $pane;
    $display->panels['center'][4] = 'new-20af9cb8-3618-4b83-937d-759e95764c47';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context_2'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'term_view_panel_context';
  $handler->task = 'term_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'System settings',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'logic' => 'and',
      'plugins' => array(
        0 => array(
          'name' => 'term_vocabulary',
          'settings' => array(
            'machine_name' => array(
              'system_settings' => 'system_settings',
            ),
          ),
          'context' => 'argument_term_1',
          'not' => FALSE,
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '6a69c6e3-778e-4a95-86e6-fb7d91aeb9d4';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-8183e8be-5a0f-48c2-9bb9-cc635f423d66';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'taxonomy_term:field_text';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_term_1',
      'override_title' => 1,
      'override_title_text' => '配置值',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8183e8be-5a0f-48c2-9bb9-cc635f423d66';
    $display->content['new-8183e8be-5a0f-48c2-9bb9-cc635f423d66'] = $pane;
    $display->panels['center'][0] = 'new-8183e8be-5a0f-48c2-9bb9-cc635f423d66';
    $pane = new stdClass();
    $pane->pid = 'new-8b2993f5-b03b-4c34-aa2c-fcaf5d8e5c14';
    $pane->panel = 'center';
    $pane->type = 'term_description';
    $pane->subtype = 'term_description';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_term_1',
      'override_title' => 1,
      'override_title_text' => '说明',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '8b2993f5-b03b-4c34-aa2c-fcaf5d8e5c14';
    $display->content['new-8b2993f5-b03b-4c34-aa2c-fcaf5d8e5c14'] = $pane;
    $display->panels['center'][1] = 'new-8b2993f5-b03b-4c34-aa2c-fcaf5d8e5c14';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-8183e8be-5a0f-48c2-9bb9-cc635f423d66';
  $handler->conf['display'] = $display;
  $export['term_view_panel_context'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function tvlib_feature_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'create_image';
  $page->task = 'page';
  $page->admin_title = 'Create image';
  $page->admin_description = '';
  $page->path = 'create-image';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'create announcement content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'create book content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      2 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'create info content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      3 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'create poster content',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'or',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_create_image_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'create_image';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '添加图片';
  $display->uuid = 'a38d6693-6e87-4a54-9620-226ac4dded48';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-aab293a3-04da-46c9-aa20-87070527617c';
    $pane->panel = 'center';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<a href=\'node/add/info\'>内容</a>
<a href=\'node/add/announcement\'>公告</a>
<a href=\'node/add/book\'>图书</a>
<a href=\'node/add/poster\'>海报</a>',
      'format' => 'filtered_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'aab293a3-04da-46c9-aa20-87070527617c';
    $display->content['new-aab293a3-04da-46c9-aa20-87070527617c'] = $pane;
    $display->panels['center'][0] = 'new-aab293a3-04da-46c9-aa20-87070527617c';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['create_image'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'create_video_audio';
  $page->task = 'page';
  $page->admin_title = 'Create video audio';
  $page->admin_description = '';
  $page->path = 'create-video-audio';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_create_video_audio_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'create_video_audio';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '添加视音频';
  $display->uuid = 'a38d6693-6e87-4a54-9620-226ac4dded48';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-aab293a3-04da-46c9-aa20-87070527617c';
    $pane->panel = 'center';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<a href=\'node/add/video\'>视频</a>
<a href=\'node/add/audio\'>音频</a>',
      'format' => 'filtered_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'aab293a3-04da-46c9-aa20-87070527617c';
    $display->content['new-aab293a3-04da-46c9-aa20-87070527617c'] = $pane;
    $display->panels['center'][0] = 'new-aab293a3-04da-46c9-aa20-87070527617c';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['create_video_audio'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'import_image';
  $page->task = 'page';
  $page->admin_title = 'Import image';
  $page->admin_description = '';
  $page->path = 'import-image';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'import announcement_import feeds',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'import book_import feeds',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      2 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'import info_import feeds',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      3 => array(
        'name' => 'perm',
        'settings' => array(
          'perm' => 'import poster_import feeds',
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'or',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_import_image_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'import_image';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '导入图片信息';
  $display->uuid = 'a38d6693-6e87-4a54-9620-226ac4dded48';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-aab293a3-04da-46c9-aa20-87070527617c';
    $pane->panel = 'center';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<a href=\'node/add/info-import\'>内容</a>
<a href=\'node/add/announcement-import\'>公告</a>
<a href=\'node/add/book-import\'>图书</a>
<a href=\'node/add/poster-import\'>海报</a>',
      'format' => 'filtered_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'aab293a3-04da-46c9-aa20-87070527617c';
    $display->content['new-aab293a3-04da-46c9-aa20-87070527617c'] = $pane;
    $display->panels['center'][0] = 'new-aab293a3-04da-46c9-aa20-87070527617c';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['import_image'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'mark_content_deleted';
  $page->task = 'page';
  $page->admin_title = 'Mark content deleted';
  $page->admin_description = '';
  $page->path = 'node/%node/content-delete';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'entity_field_value:node:video:field_status',
        'settings' => array(
          'field_status' => array(
            'und' => array(
              0 => array(
                'value' => '未审核',
              ),
            ),
          ),
          'field_status_value' => '未审核',
        ),
        'context' => 'argument_entity_id:node_1',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'entity_field_value:node:video:field_status',
        'settings' => array(
          'field_status' => array(
            'und' => array(
              0 => array(
                'value' => '审核不通过',
              ),
            ),
          ),
          'field_status_value' => '审核不通过',
        ),
        'context' => 'argument_entity_id:node_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'or',
  );
  $page->menu = array();
  $page->arguments = array(
    'node' => array(
      'id' => 1,
      'identifier' => 'Node: ID',
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_mark_content_deleted_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'mark_content_deleted';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '确定删除 %node:content-type %node:title 吗？';
  $display->uuid = '208c5e3a-a62e-4506-ba09-19ea5ba391ea';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-e45b6d91-8414-4a77-94f5-5738b787b7aa';
    $pane->panel = 'center';
    $pane->type = 'rules_panes';
    $pane->subtype = 'rules_update_content_deletion_remarks';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'button_text' => '删除',
      'help_text' => '',
      'component' => 'rules_update_content_deletion_remarks',
      'context' => array(
        0 => 'argument_entity_id:node_1',
        1 => 'empty',
      ),
      'override_title_text' => '此操作无法恢复',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e45b6d91-8414-4a77-94f5-5738b787b7aa';
    $display->content['new-e45b6d91-8414-4a77-94f5-5738b787b7aa'] = $pane;
    $display->panels['center'][0] = 'new-e45b6d91-8414-4a77-94f5-5738b787b7aa';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['mark_content_deleted'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'welcome';
  $page->task = 'page';
  $page->admin_title = 'Welcome';
  $page->admin_description = '';
  $page->path = 'welcome';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_welcome_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'welcome';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'User',
        'keyword' => 'user',
        'name' => 'user',
        'type' => 'current',
        'uid' => '',
        'id' => 1,
      ),
    ),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '9628c1ab-7e0e-4304-8fb4-6c9f3e024758';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-565d9af4-c028-4f63-ba2d-2c590834131c';
    $pane->panel = 'center';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 1,
            ),
          ),
          'context' => 'context_user_1',
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<h1>
欢迎使用佛山图书馆
电视图书馆系统内容管理平台
</h1>

%user:name 已登陆成功
<a href=\'user/logout\'>退出</a>',
      'format' => 'full_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '565d9af4-c028-4f63-ba2d-2c590834131c';
    $display->content['new-565d9af4-c028-4f63-ba2d-2c590834131c'] = $pane;
    $display->panels['center'][0] = 'new-565d9af4-c028-4f63-ba2d-2c590834131c';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['welcome'] = $page;

  return $pages;

}
