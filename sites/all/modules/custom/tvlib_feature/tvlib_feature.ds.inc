<?php
/**
 * @file
 * tvlib_feature.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function tvlib_feature_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|announcement|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'announcement';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'approve' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:3:{s:7:"context";a:1:{i:0;s:25:"argument_entity_id:node_1";}s:14:"override_title";i:1;s:19:"override_title_text";s:0:"";}s:4:"type";s:11:"panels_mini";s:7:"subtype";s:16:"content_approval";}',
        'load_terms' => 0,
      ),
    ),
  );
  $export['node|announcement|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|audio|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'audio';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'approve' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:3:{s:7:"context";a:1:{i:0;s:25:"argument_entity_id:node_1";}s:14:"override_title";i:1;s:19:"override_title_text";s:0:"";}s:4:"type";s:11:"panels_mini";s:7:"subtype";s:16:"content_approval";}',
        'load_terms' => 0,
      ),
    ),
  );
  $export['node|audio|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|book|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'book';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'approve' => array(
      'weight' => '12',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:3:{s:7:"context";a:1:{i:0;s:25:"argument_entity_id:node_1";}s:14:"override_title";i:1;s:19:"override_title_text";s:0:"";}s:4:"type";s:11:"panels_mini";s:7:"subtype";s:16:"content_approval";}',
        'load_terms' => 0,
      ),
    ),
  );
  $export['node|book|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|info|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'info';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'approve' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:3:{s:7:"context";a:1:{i:0;s:25:"argument_entity_id:node_1";}s:14:"override_title";i:1;s:19:"override_title_text";s:0:"";}s:4:"type";s:11:"panels_mini";s:7:"subtype";s:16:"content_approval";}',
        'load_terms' => 0,
      ),
    ),
  );
  $export['node|info|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|poster|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'poster';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'approve' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:3:{s:7:"context";a:1:{i:0;s:25:"argument_entity_id:node_1";}s:14:"override_title";i:1;s:19:"override_title_text";s:0:"";}s:4:"type";s:11:"panels_mini";s:7:"subtype";s:16:"content_approval";}',
        'load_terms' => 0,
      ),
    ),
  );
  $export['node|poster|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|video|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'video';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'approve' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:3:{s:7:"context";a:1:{i:0;s:25:"argument_entity_id:node_1";}s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}s:4:"type";s:11:"panels_mini";s:7:"subtype";s:16:"content_approval";}',
        'load_terms' => 0,
      ),
    ),
  );
  $export['node|video|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'taxonomy_term|category|default';
  $ds_fieldsetting->entity_type = 'taxonomy_term';
  $ds_fieldsetting->bundle = 'category';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'distribute' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:3:{s:7:"context";a:1:{i:0;s:34:"argument_entity_id:taxonomy_term_1";}s:14:"override_title";i:1;s:19:"override_title_text";s:0:"";}s:4:"type";s:11:"panels_mini";s:7:"subtype";s:15:"term_distribute";}',
        'load_terms' => 0,
      ),
    ),
  );
  $export['taxonomy_term|category|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function tvlib_feature_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'approve';
  $ds_field->label = '审核';
  $ds_field->field_type = 7;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'default' => array(),
    'settings' => array(
      'show_title' => array(
        'type' => 'checkbox',
      ),
      'title_wrapper' => array(
        'type' => 'textfield',
        'description' => '例如: h1, h2, p',
      ),
      'ctools' => array(
        'type' => 'ctools',
      ),
    ),
  );
  $export['approve'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'distribute';
  $ds_field->label = 'Distribute';
  $ds_field->field_type = 7;
  $ds_field->entities = array(
    'taxonomy_term' => 'taxonomy_term',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'default' => array(),
    'settings' => array(
      'show_title' => array(
        'type' => 'checkbox',
      ),
      'title_wrapper' => array(
        'type' => 'textfield',
        'description' => 'Eg: h1, h2, p',
      ),
      'ctools' => array(
        'type' => 'ctools',
      ),
    ),
  );
  $export['distribute'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function tvlib_feature_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|announcement|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'announcement';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_reader_number',
        1 => 'field_content',
        2 => 'field_start_time',
        3 => 'field_end_time',
        4 => 'field_status',
        5 => 'field_er_user',
        6 => 'field_reject_remarks',
        7 => 'field_unpublish_remarks',
        8 => 'field_deletion_remarks',
        9 => 'field_tr_category',
        10 => 'approve',
      ),
    ),
    'fields' => array(
      'field_reader_number' => 'ds_content',
      'field_content' => 'ds_content',
      'field_start_time' => 'ds_content',
      'field_end_time' => 'ds_content',
      'field_status' => 'ds_content',
      'field_er_user' => 'ds_content',
      'field_reject_remarks' => 'ds_content',
      'field_unpublish_remarks' => 'ds_content',
      'field_deletion_remarks' => 'ds_content',
      'field_tr_category' => 'ds_content',
      'approve' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|announcement|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|audio|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'audio';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_file',
        1 => 'field_description',
        2 => 'field_status',
        3 => 'field_er_user',
        4 => 'field_unpublish_remarks',
        5 => 'field_deletion_remarks',
        6 => 'field_tr_category',
        7 => 'approve',
      ),
    ),
    'fields' => array(
      'field_file' => 'ds_content',
      'field_description' => 'ds_content',
      'field_status' => 'ds_content',
      'field_er_user' => 'ds_content',
      'field_unpublish_remarks' => 'ds_content',
      'field_deletion_remarks' => 'ds_content',
      'field_tr_category' => 'ds_content',
      'approve' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|audio|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|book|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'book';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image',
        1 => 'field_author',
        2 => 'field_publisher',
        3 => 'field_call_number',
        4 => 'field_description',
        5 => 'field_repository',
        6 => 'field_status',
        7 => 'field_er_user',
        8 => 'field_reject_remarks',
        9 => 'field_unpublish_remarks',
        10 => 'field_deletion_remarks',
        11 => 'field_tr_category',
        12 => 'approve',
      ),
    ),
    'fields' => array(
      'field_image' => 'ds_content',
      'field_author' => 'ds_content',
      'field_publisher' => 'ds_content',
      'field_call_number' => 'ds_content',
      'field_description' => 'ds_content',
      'field_repository' => 'ds_content',
      'field_status' => 'ds_content',
      'field_er_user' => 'ds_content',
      'field_reject_remarks' => 'ds_content',
      'field_unpublish_remarks' => 'ds_content',
      'field_deletion_remarks' => 'ds_content',
      'field_tr_category' => 'ds_content',
      'approve' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|book|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|info|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'info';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image',
        1 => 'field_status',
        2 => 'field_er_user',
        3 => 'field_reject_remarks',
        4 => 'field_unpublish_remarks',
        5 => 'field_deletion_remarks',
        6 => 'field_tr_category',
        7 => 'approve',
      ),
    ),
    'fields' => array(
      'field_image' => 'ds_content',
      'field_status' => 'ds_content',
      'field_er_user' => 'ds_content',
      'field_reject_remarks' => 'ds_content',
      'field_unpublish_remarks' => 'ds_content',
      'field_deletion_remarks' => 'ds_content',
      'field_tr_category' => 'ds_content',
      'approve' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|info|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|poster|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'poster';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image',
        1 => 'field_status',
        2 => 'field_er_user',
        3 => 'field_reject_remarks',
        4 => 'field_unpublish_remarks',
        5 => 'field_deletion_remarks',
        6 => 'field_tr_category',
        7 => 'approve',
      ),
    ),
    'fields' => array(
      'field_image' => 'ds_content',
      'field_status' => 'ds_content',
      'field_er_user' => 'ds_content',
      'field_reject_remarks' => 'ds_content',
      'field_unpublish_remarks' => 'ds_content',
      'field_deletion_remarks' => 'ds_content',
      'field_tr_category' => 'ds_content',
      'approve' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|poster|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|video|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'video';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_video',
        1 => 'field_image',
        2 => 'field_description',
        3 => 'field_status',
        4 => 'field_er_user',
        5 => 'field_reject_remarks',
        6 => 'field_unpublish_remarks',
        7 => 'field_deletion_remarks',
        8 => 'field_video_remarks',
        9 => 'field_tr_category',
        10 => 'approve',
      ),
    ),
    'fields' => array(
      'field_video' => 'ds_content',
      'field_image' => 'ds_content',
      'field_description' => 'ds_content',
      'field_status' => 'ds_content',
      'field_er_user' => 'ds_content',
      'field_reject_remarks' => 'ds_content',
      'field_unpublish_remarks' => 'ds_content',
      'field_deletion_remarks' => 'ds_content',
      'field_video_remarks' => 'ds_content',
      'field_tr_category' => 'ds_content',
      'approve' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|video|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'taxonomy_term|category|default';
  $ds_layout->entity_type = 'taxonomy_term';
  $ds_layout->bundle = 'category';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'description',
        1 => 'field_status',
        2 => 'distribute',
      ),
    ),
    'fields' => array(
      'description' => 'ds_content',
      'field_status' => 'ds_content',
      'distribute' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['taxonomy_term|category|default'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function tvlib_feature_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'thumbnail';
  $ds_view_mode->label = 'Thumbnail';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['thumbnail'] = $ds_view_mode;

  return $export;
}
