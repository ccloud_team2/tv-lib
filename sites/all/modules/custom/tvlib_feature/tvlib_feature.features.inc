<?php
/**
 * @file
 * tvlib_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function tvlib_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "backup_migrate" && $api == "backup_migrate_exportables") {
    return array("version" => "1");
  }
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function tvlib_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function tvlib_feature_image_default_styles() {
  $styles = array();

  // Exported image style: book_icon.
  $styles['book_icon'] = array(
    'name' => 'book_icon',
    'label' => 'Book icon (166x220)',
    'effects' => array(
      9 => array(
        'label' => '改变尺寸',
        'help' => '改变尺寸将会给图片一个准确的宽高尺寸。这可能会让图片拉伸或挤压而导致变形。',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => 166,
          'height' => 220,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: pic_icon.
  $styles['pic_icon'] = array(
    'name' => 'pic_icon',
    'label' => 'Pic icon (1127x421)',
    'effects' => array(
      6 => array(
        'label' => '改变尺寸',
        'help' => '改变尺寸将会给图片一个准确的宽高尺寸。这可能会让图片拉伸或挤压而导致变形。',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => 1127,
          'height' => 421,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: poster_icon.
  $styles['poster_icon'] = array(
    'name' => 'poster_icon',
    'label' => 'Poster icon (225x300)',
    'effects' => array(
      2 => array(
        'label' => '改变尺寸',
        'help' => '改变尺寸将会给图片一个准确的宽高尺寸。这可能会让图片拉伸或挤压而导致变形。',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => 225,
          'height' => 300,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: video_icon.
  $styles['video_icon'] = array(
    'name' => 'video_icon',
    'label' => 'Video icon (264x176)',
    'effects' => array(
      8 => array(
        'label' => '改变尺寸',
        'help' => '改变尺寸将会给图片一个准确的宽高尺寸。这可能会让图片拉伸或挤压而导致变形。',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => 264,
          'height' => 176,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function tvlib_feature_node_info() {
  $items = array(
    'announcement' => array(
      'name' => t('公告'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'announcement_import' => array(
      'name' => t('公告导入'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
    'audio' => array(
      'name' => t('音频'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'book' => array(
      'name' => t('图书'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('书名'),
      'help' => '',
    ),
    'book_import' => array(
      'name' => t('图书导入'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'info' => array(
      'name' => t('内容'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'info_import' => array(
      'name' => t('内容导入'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'poster' => array(
      'name' => t('海报'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
    'poster_import' => array(
      'name' => t('海报导入'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
    'video' => array(
      'name' => t('视频'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('名称'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_video_default_presets().
 */
function tvlib_feature_video_default_presets() {
  $items = array(
    'H.264 mp4' => array(
      'name' => 'H.264 mp4',
      'description' => 'mp4 video with h.264 codec',
      'settings' => array(
        'video_extension' => 'mp4',
        'video_codec' => 'h264',
        'video_preset' => '',
        'video_quality' => 3,
        'video_speed' => 3,
        'wxh' => '720x480',
        'video_aspectmode' => 'preserve',
        'video_upscale' => 0,
        'audio_codec' => 'aac',
        'audio_quality' => 3,
        'deinterlace' => 'detect',
        'max_frame_rate' => '',
        'frame_rate' => '',
        'keyframe_interval' => '',
        'video_bitrate' => 900,
        'bitrate_cap' => 1800,
        'buffer_size' => '',
        'one_pass' => 0,
        'skip_video' => 0,
        'pixel_format' => 'yuv420p',
        'h264_profile' => 'high',
        'audio_bitrate' => '',
        'audio_channels' => 2,
        'audio_sample_rate' => '',
        'skip_audio' => 0,
        'video_watermark_enabled' => 0,
        'video_watermark_fid' => 0,
        'video_watermark_x' => 5,
        'video_watermark_y' => 5,
        'video_watermark_width' => '',
        'video_watermark_height' => '',
        'video_watermark_origin' => 'content',
        'autolevels' => 0,
        'deblock' => 0,
        'denoise' => '',
        'clip_start' => '',
        'clip_length' => '',
      ),
    ),
  );
  return $items;
}
