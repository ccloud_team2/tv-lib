<?php
/**
 * @file
 * tvlib_feature.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function tvlib_feature_filter_default_formats() {
  $formats = array();

  // Exported format: 文本编辑.
  $formats['editor'] = array(
    'format' => 'editor',
    'name' => '文本编辑',
    'cache' => 1,
    'status' => 1,
    'weight' => -10,
    'filters' => array(),
  );

  // Exported format: 无格式文本编辑.
  $formats['editor_no_format'] = array(
    'format' => 'editor_no_format',
    'name' => '无格式文本编辑',
    'cache' => 1,
    'status' => 1,
    'weight' => -9,
    'filters' => array(
      'filter_autop' => array(
        'weight' => -49,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_html' => array(
        'weight' => -48,
        'status' => 1,
        'settings' => array(
          'allowed_html' => '<p> <br>',
          'filter_html_help' => 0,
          'filter_html_nofollow' => 0,
        ),
      ),
    ),
  );

  return $formats;
}
