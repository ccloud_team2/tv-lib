<?php
/**
 * @file
 * tvlib_feature.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function tvlib_feature_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'announcement_import';
  $feeds_importer->config = array(
    'name' => '公告导入',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'csv',
        'direct' => 0,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => '名称',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => '读者认证号',
            'target' => 'field_reader_number',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => '公告内容',
            'target' => 'field_content',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => '开始时间',
            'target' => 'field_start_time:start',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => '结束时间',
            'target' => 'field_end_time:start',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => '分类',
            'target' => 'field_tr_category',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'announcement',
      ),
    ),
    'content_type' => 'announcement_import',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['announcement_import'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'book_import';
  $feeds_importer->config = array(
    'name' => '图书导入',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'csv',
        'direct' => 0,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => '书名',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => '作者',
            'target' => 'field_author',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => '出版社',
            'target' => 'field_publisher',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => '索书号',
            'target' => 'field_call_number',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => '图书简介',
            'target' => 'field_description',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => '馆藏地',
            'target' => 'field_repository',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => '排序',
            'target' => 'field_sort',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => '分类',
            'target' => 'field_tr_category',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'editor',
        'skip_hash_check' => 0,
        'bundle' => 'book',
      ),
    ),
    'content_type' => 'book_import',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['book_import'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'info_import';
  $feeds_importer->config = array(
    'name' => '内容导入',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'csv',
        'direct' => 0,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => '名称',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => '分类',
            'target' => 'field_tr_category',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'info',
      ),
    ),
    'content_type' => 'info_import',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['info_import'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'poster_import';
  $feeds_importer->config = array(
    'name' => '海报导入',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'csv',
        'direct' => 0,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => '名称',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => '排序',
            'target' => 'field_sort',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => '分类',
            'target' => 'field_tr_category',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'poster',
      ),
    ),
    'content_type' => 'poster_import',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['poster_import'] = $feeds_importer;

  return $export;
}
